const apiCallRequest = require('./request')
const apiCallNode = require('./nodejs')

const http = require('http')

http.createServer((req, res) => {
     if(req.url === "/dogs/hound") {
        apiCallNode.callApi(function(response) {
            res.write(response);
            res.end();
        })
    }
}).listen(5000);

console.log("Service run on port 5000");