const request = require('request');

URL = 'https://dog.ceo/api/breed/hound/list';

const callApiUsingRequest = (callback) => {
    request(URL, {json: true}, (err, res, body) => {
        if (err) {
            return callback(err);
        }
        return callback(body);
    });

    request(URL, {json: true}, (err, res, body) => {
        if (err) {
            return callback(err);
        }
        return callback(body);
    });
}

module.exports.callApi = callApiUsingRequest;